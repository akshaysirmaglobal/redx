export const ADD_INC = (
    payload,
)=> ({
    type : 'increment',
    payload,
});

export const ADD_DEC = (
  payload,
)=> ({
  type : 'decrement',
  payload,
});